<?php
if (!defined('INAPP')) {
    header('Location: /index.php');
}
//Production vs. Developtment
define('DEBUG', true); //set to false when in production mode

//Default Range for displayed completed requests
define('COMPLETED_RANGE', time()-604800);

//Path Constants
define('ROOT', __DIR__);  // DO NOT CHANGE
define('SLASH', DIRECTORY_SEPARATOR); //DO NOT CHANGE
define('PATH_TO_AVATARS', "view".SLASH."img".SLASH."avatars".SLASH);

//User Levels // These probably won't change but will make it easier to remember what numbers go with what level
define('SUPER_ADMIN', 99); //can do everything
define('CLIENT', 90); // can do everything with corresponding client account scheduling and account info and all admin functions linked to client.
define('ADMIN', 80); // can do everything with request system of client account and most admin functions
define('MAINT', 70); //can view, accept, comment, add, and complete requests
define('MANAGER', 60); //can view, comment, and add requests
define('FRONT_DESK', 50); // can view, comment, and add requests
define('HSKP', 40); // can view, commend, and add requests
define('VIEWER', 30);  // can view requests

//Message Statuses
define('MSG_UNREAD', 10);
define('MSG_READ', 7);
define('MSG_ARCHIVED', 4);

//Request Statuses
define('RQ_DELETED', 80);
define('RQ_CANCELLED', 70);
define('RQ_COMPLETED', 60);
define('RQ_DEFERRED', 50);
define('RQ_STARTED', 40);
define('RQ_ACCEPTED', 30);
define('RQ_ASSIGNED', 20);
define('RQ_REPORTED', 10);


//Request Levels
define('URGENT', 50);
define('IMPORTANT', 40);
define('COMMON', 30);
define('DAILY_TASK', 20);
define('SCHEDULED_TASK', 10);
define('PM_TASK', 10);
 ?>
