<?php
if (!defined('INAPP')) {
    header('Location: /index.php');
}
//AUTOLOAD CLASSES (All classes should go in /classes folder with a .class.php extension.  This will allow for namespaces)
spl_autoload_register('myAutoLoader');
function myAutoLoader($classname){
    $path = 'classes';
    $ext = '.class.php';
    //$fullPath = ROOT.SLASH.$path.SLASH.$classname.$ext;
    $fullPath = ROOT.SLASH.$path.SLASH.$classname.$ext;
    if (!file_exists($fullPath)){
        return false;
    }
    require_once $fullPath;
}
