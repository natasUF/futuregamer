<?php
class User
{
    public $userID;
    public $userClientID;
    public $userFName;
    public $userLName;
    public $userName;
    public $userAbout;
    public $userCity;
    public $userState;
    //public $userZIP; //Might not need
    public $userOccupation;
    public $userURL;
    public $userEmail;
    public $userPhone;
    public $userLevel;
    public $userAvatar;
    public $userPropertyName;
    public $userPropertyCode;
    public $tempPass;
    protected $tempHash;

    public static function generatePassword($length = 9, $add_dashes = false, $available_sets = 'lud')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        echo "DASH STRING = ".$dash_str."</br>";
        return $dash_str;
    }

    public static function getUserdata($pdo, $user)
    {
        $stmt = $pdo->prepare("SELECT * FROM users WHERE user_id=?");
        $stmt->execute([$user]);
        while ($row = $stmt->fetch()) {
            $userInfo['ID'] = $row['user_id'];
            $userInfo['FName'] = $row['user_fname'];
            $userInfo['LName'] = $row['user_lname'];
            $userInfo['Name'] = $row['user_fname'].' '.$row['user_lname'];
            $userInfo['Email'] = $row['user_email'];
            $userInfo['City'] = $row['user_city'];
            $userInfo['State'] = $row['user_state'];
            //$this->userZIP = $row['user_ZIP'];  //Might not need
            $userInfo['About'] = $row['user_about'];
            $userInfo['Occupation'] = $row['user_occupation'];
            $userInfo['URL'] = $row['user_url'];
            $userInfo['Phone'] = $row['user_phone'];
            $userInfo['Level'] = $row['user_level'];
            $userInfo['Avatar'] = $row['user_avatar'];
            $userInfo['ClientID'] = $row['user_client'];
            $propertyarray= explode(',', filter_var(ltrim($row['user_additional_properties'])));
        }
        for ($i=0; $i<count($propertyarray); $i++) {
            $stmt2 = $pdo->prepare("SELECT * FROM properties WHERE property_id = ?");
            $stmt2->execute([$propertyarray[$i]]);
            while ($row2 = $stmt2->fetch()) {
                if($i===0){
                    $userInfo['PropertyID'] = $row2['property_id'];
                    $userInfo['PropertyName'] = $row2['property_name'];
                    $userInfo['PropertyCode'] = $row2['property_code'];
                }
                $userInfo['AdditionalProperties'][$i][0] = $row2['property_id'];
                $userInfo['AdditionalProperties'][$i][1] = $row2['property_name'];
            }
        }
            return $userInfo;

    }

    public static function getName($pdo,$user){
        $stmt = $pdo->prepare("SELECT user_fname, user_lname FROM users WHERE user_id=?");
        $stmt->execute([$user]);
        while ($row = $stmt->fetch()) {
            $fname=$row['user_fname'];
            $lname=$row['user_lname'];
            $username=$fname." ".$lname;
            return $username;
        }
        return false;
    }


    public static function getLevel($pdo,$user){
        $stmt = $pdo->prepare("SELECT user_level FROM users WHERE user_id=?");
        $stmt->execute([$user]);
        while ($row = $stmt->fetch()) {
            return $row['user_level'];
        }
        return false;
    }

    public static function getHomeProperty($pdo,$user){
        $stmt = $pdo->prepare("SELECT user_additional_properties FROM users WHERE user_id=?");
        $stmt->execute([$user]);
        while ($row = $stmt->fetch()) {
            $pieces = explode(",", $row['user_additional_properties']);
            $home = $pieces[0];
            return $home;
        }
        return false;
    }
    public static function getAddtlProperties($pdo,$user){
        $stmt = $pdo->prepare("SELECT user_additional_properties FROM users WHERE user_id=?");
        $stmt->execute([$user]);
        $property=false;
        while ($row = $stmt->fetch()) {
            $pieces = explode(",", $row['user_additional_properties']);
            $i=0;$j=0;
            foreach ($pieces as $piece) {
                if($i !== 0){
                $property[$j]=$piece;
                $j++;
            }
            $i++;
            //$home = $pieces[0];
            }
            return $property;
        }
        return false;
    }


    public function getUserNotifications($pdo, $user)
    {
        $stmt = $pdo->prepare("SELECT * FROM users LEFT JOIN alerts on users.user_id=alerts.user_id WHERE users.user_id=?");
        //$stmt = $pdo->prepare("SELECT * FROM users WHERE user_id=?");
        $stmt->execute([$user]);
        while ($row = $stmt->fetch()) {
        }
    }

    public static function addUser($pdo, $postvars, $tempPass)
    {
        $client = $postvars['addUser'];
        $property = $postvars['selectproperty'];
        $fName = $postvars['fname'];
        $lName = $postvars['lname'];
        $email = $postvars['email'];
        $level = $postvars['selectrole'];
        $avatar = 'genericavatar.png';

        $passhash = password_hash($tempPass, PASSWORD_DEFAULT);

        $tempHash = md5(rand(0, 1000)); // Generate random 32 character hash

        $stmt = $pdo->prepare("SELECT * FROM users WHERE user_email=?");
        $stmt->execute([$email]);
        $userExists = $stmt->fetch();
        if (!$userExists) {
            try {
                $pdo->beginTransaction();
                $sql = "INSERT INTO users (user_email, user_pass, user_client, user_fname, user_lname, user_level, user_additional_properties, user_active, user_hash, user_avatar) VALUES (?,?,?,?,?,?,?,?,?,?)";
                $stmt = $pdo->prepare($sql);
                $stmt->execute([$email, $passhash, $client, $fName, $lName, $level, $property, '0', $tempHash, $avatar]);
                $pdo->commit();
            } catch (Exception $e) {
                $pdo->rollback();
                throw $e;
            }
            return true;
        } else {
            return false;
        }
    }

    public static function sendVerifyMail($pdo, $email, $tempPass, $grouped)
    {
        //SEND ACTIVATION EMAIL
        $to_email = $email;
        $stmt = $pdo->prepare("SELECT * FROM users WHERE user_email=?");
        $stmt->execute([$to_email]);
        $row = $stmt->fetch();
        //$stmt = $pdo->prepare("SELECT user_hash FROM users WHERE user_email=?");
        //$stmt->execute([$to_email]);
        //$this->tempHash = $stmt->fetchColumn();

        $subject = 'Set Up your Inngine Account';
        $message = '<p>An administrator at your property has entered this email to be a user ';
        $message .= 'on inngine.com.  Please click the below link to finish setting up ';
        $message .='your account.</p></br>';
        $message .='<p>Please click this link to activate your account using the following credentials:</p>';
        $message .='<p>------------------------</p>';
        $message .='<p>Login: '.$to_email.'</p>';
        $message .="<p>Password: ".$tempPass."</p>";
        $message .='<p>------------------------</p>';
        $message .='<p>Please change your password after logging in.</p>';
        $message .='<a href="https://inngine.com/verify/'.$to_email.'/'.$row['user_hash'].'/'.$grouped.'">VERIFY EMAIL</a>';
        $message = wordwrap($message, 70);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: admin@inngine.com';
        mail($to_email, $subject, $message, $headers);
    }

    public static function changePass($pdo, $postvars, $userEmail)
    {
        if (filter_has_var(INPUT_POST, 'currentpassword')) {
            $currentpass = filter_input(INPUT_POST, 'currentpassword', FILTER_SANITIZE_STRING); //TODO This is depracated.
            $currentpass = filter_var($currentpass, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (filter_has_var(INPUT_POST, 'newpassword')) {
            $newpass = filter_input(INPUT_POST, 'newpassword', FILTER_SANITIZE_STRING); //TODO This is depracated.
            $newpass = filter_var($newpass, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (filter_has_var(INPUT_POST, 'repeatpassword')) {
            $repeat = filter_input(INPUT_POST, 'repeatpassword', FILTER_SANITIZE_STRING); //TODO This is depracated.
            $repeat = filter_var($repeat, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if($newpass !== $repeat){
            return "Passwords must match.";
        }
        $stmt = $pdo->prepare("SELECT * FROM users WHERE user_active=1 AND user_email=?");
        $stmt->execute([$userEmail]);
        while ($row = $stmt->fetch()) {
            $hash = strval($row['user_pass']);
            $fetchedPassword = substr( $hash, 0, 60 );
            if (password_verify($currentpass, $fetchedPassword)) {//check input password against database
                $hashpass = password_hash($newpass, PASSWORD_DEFAULT);
                try {
                    $pdo->beginTransaction();
                    $sql = "UPDATE users SET user_pass=? WHERE user_email=?";
                    $stmt = $pdo->prepare($sql);
                    $stmt->execute([$hashpass,$userEmail]);
                    $pdo->commit();
                    return "Password succesfully changed.";
                } catch (Exception $e) {
                    $pdo->rollback();
                    throw $e;
                }
                //$stmt = $pdo->prepare("UPDATE users SET user_pass=? WHERE user_email=?");
                //$stmt->execute([$hashpass,$userEmail]);
            } else {
                return "Incorrect Password";
            }
        }
    }

    public static function editDetails($pdo, $postvars, $email)
    {
        $email=$email;
        //$this->userFName = !empty($postvars['fname']) ? $postvars['fname'] : $this->userFName;
        //$this->userLName = !empty($postvars['lname']) ? $postvars['lname'] : $this->userLName;
        //$this->userAbout = !empty($postvars['aboutme']) ? $postvars['aboutme'] : $this->userAbout;
        $city = !empty($postvars['changecity']) ? $postvars['changecity'] : $_SESSION['userData']['City'];
        $state = !empty($postvars['changestate']) ? $postvars['changestate'] : $_SESSION['userData']['State'];
        $job = !empty($postvars['changejob']) ? $postvars['changejob'] : $_SESSION['userData']['Occupation'];
        $phone = !empty($postvars['changephone']) ? $postvars['changephone'] : $_SESSION['userData']['Phone'];
        //$this->userURL = !empty($postvars['url']) ? $postvars['url'] : $this->userURL;
        try {
            $pdo->beginTransaction();
            $sql = "UPDATE users SET user_city=?, user_state=?, user_occupation=?, user_phone=? WHERE user_email=?";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$city, $state, $job, $phone, $email]);
            $pdo->commit();
            $msg="User Info succesfully changed.  You must log out to see the changes.";
            return $msg;
        } catch (Exception $e) {
            $pdo->rollback();
            throw $e;
        }
    }

    public static function getPropertyUsers($pdo, $property){
        $stmt = $pdo->prepare("SELECT * FROM users WHERE user_property=? AND user_level<=? AND user_active=1");
        //$stmt = $pdo->prepare("SELECT * FROM users WHERE user_id=?");
        $stmt->execute([$property,ADMIN]);
        $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $row;
    }
/*
    public function getUnreadMessages($pdo, $user)
    {
        //$stmt = $pdo->prepare("SELECT msg_id FROM messages LEFT JOIN users on users.user_id=messages.msg_from WHERE msg_to=? AND msg_read=10");
        //$stmt->execute([$user]);
        $stmt = $pdo->prepare("SELECT msg_id FROM messages WHERE msg_to=? AND msg_read=?");
        $stmt->execute([$user,MSG_UNREAD]);
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $total = count($row);
        return $total;
    }
    /*
    public function getMessages($pdo, $user)
    {
        $stmt = $pdo->prepare("SELECT msg_id, msg_timestamp, msg_body, msg_from, msg_read FROM messages LEFT JOIN users on users.user_id=messages.msg_from WHERE msg_to=?");
        $stmt->execute([$user]);
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (empty($row)){
            return false;
        }
        else {
            return $row;
        }
    }
    */

    public static function uploadAvatar($pdo, $userID){
        if($_FILES['uploadavatar']['error'] !== 4) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime=finfo_file($finfo, $_FILES["uploadavatar"]["tmp_name"]);
            $array = explode("/",$mime);
            if($array[0] == "image"){
                $imageFileType = explode(".",$array[1]);
                if(count($imageFileType) == 1){
                    if($imageFileType[0] == "jpg" || $imageFileType[0] == "jpeg" || $imageFileType[0] == "png"){
                        if ($_FILES["uploadavatar"]["size"] < 100000) {
                            $uploadOk = 1;
                            $avatarName = $userID."_avatar.".$imageFileType[0];
                            $moveFile = move_uploaded_file($_FILES["uploadavatar"]["tmp_name"], "view/img/avatars/".$avatarName);
                            if ($moveFile){
                                $alert_message = "Succesfull Upload.  You must log out to see changes.";
                            };
                            try {
                                $pdo->beginTransaction();
                                $sql = "UPDATE users SET user_avatar=? WHERE user_id=?";
                                $stmt = $pdo->prepare($sql);
                                $stmt->execute([$avatarName, $userID]);
                                $pdo->commit();
                            } catch (Exception $e) {
                                $pdo->rollback();
                                throw $e;
                            }
                        } else{
                            $alert_message = "Uploaded avatars must be less than 50Mb";
                        }
                    } else {
                        $alert_message = "Uploaded avatars must be either .jpg or .png formats";
                    }
                } else {
                    $alert_message="Stop trying to upload weird files";
                }
            } else {
                $alert_message="Uploaded file is not an image.";
            }
            finfo_close($finfo);
        }
        else {
            $alert_message="No file chosen.";
        }
        return $alert_message;
        }
    }
