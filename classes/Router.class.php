<?php
class Router {
    public function __construct(public string $currentPage = "dashboard.php")
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $url = explode('/', filter_var(ltrim($_SERVER['REQUEST_URI'], '/')), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $currentpage = $url[0].'.php';
            $file = ROOT.SLASH.'view'.SLASH.$currentpage;
            if (file_exists($file)) {
                $this->currentPage = $currentpage;  // TODO make sure malicious code can't be injected manually into URL for params
                if (!empty($url[1])) {
                    $this->param1 = $url[1];
                    $this->param1 = str_replace('%20', '&nbsp', $this->param1);
                }else{
                    $this->param1 = 'default';
                }
                if (!empty($url[2])) {
                    $this->param2 = $url[2];
                    $this->param2 = str_replace('%20', '&nbsp', $this->param2);
                }else{
                    $this->param2 = null;
                }
                if (!empty($url[3])) {
                    $this->param3 = $url[3];
                    $this->param3 = str_replace('%20', '&nbsp', $this->param3);
                }else{
                    $this->param3 = null;
                }
            } else {
                $this->currentPage = "dashboard.php";
            }
        }
    }
}
