<?php
class Login
{
    private string $inputEmail;
    private string $inputPassword;

    public function __construct(
        public object $pdo,
        public string $error ="")
    {
        $this->validateCredentials($pdo);
    }

    private function checkEmail()
    {
        if (filter_has_var(INPUT_POST, 'email')) {
            $this->inputEmail = $_POST['email'];
            if (filter_var($this->inputEmail, FILTER_VALIDATE_EMAIL)) {
                if (filter_var($this->inputEmail, FILTER_SANITIZE_EMAIL)) {
                    return $this->inputEmail;//
                }
            }
        }
    }

    private function checkPassword()
    {
        if (filter_has_var(INPUT_POST, 'password')) {
            $this->inputPassword = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING); //TODO This is depracated.
            $this->inputPassword = filter_var($this->inputPassword, FILTER_SANITIZE_SPECIAL_CHARS);
            //$this->parsedPassword = password_hash($this->inputPassword, PASSWORD_BCRYPT);
            return $this->inputPassword;
        }
    }

    public function validateCredentials($pdo)
    {
        if (filter_has_var(INPUT_POST, 'submitlogin')) {
            $this->inputEmail=$this->checkEmail();
            $this->inputPassword=$this->checkPassword();
            $stmt = $pdo->prepare("SELECT * FROM users WHERE user_active=1 AND user_email=?");//TODO may add a check if client_active = true
            $stmt->execute([$this->inputEmail]);
            while ($row = $stmt->fetch()) {
                $this->fetchedPassword = $row['user_pass'];
                if (password_verify($this->inputPassword, $this->fetchedPassword)) {
                    $_SESSION['loggedin']=true;
                    //$_SESSION['user'] = $row['user_id'];
                    $_SESSION['userData']=User::getUserdata($pdo, $row['user_id']);
                    $_SESSION['selectedProperty'] = $_SESSION['userData']['PropertyID'];
                    header('Location: /dashboard');
                }
            }
            $this->error = "Invalid Credentials";
        }
    }
}
