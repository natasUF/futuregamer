<?php
session_start();
//header('Strict-Transport-Security: max-age=31536000');
//header('Content-Security-Policy: upgrade-insecure-requests');
//$_SESSION['count']=$_SESSION['count']+1; //TODO remove after testing
//Use this on any indluded pages to prevent file fishing
define('INAPP', true); //DO NOT CHANGE OR MOVE INTO GLOBALS FILE  

require('globals.php');
require('config.php');
require('autoloader.php');//autoload classes from 'class' folder with '.class.php' extension
require('helpers.php');
$router = new Router();
$pdo = Dbh::connect();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    $loggedIn = true;
    //$property = new Property($pdo);
    //$unreadMessages=Messages::getUnreadMessages($pdo, $_SESSION['user']);
    //$userMessages=$currentUser->getMessages($pdo, $_SESSION['user']);
    //$userMessages=Messages::getMessages($pdo, $_SESSION['user']);
    //$rooms = $prop->getRooms($pdo, $currentUser->userPropertyID);
    //$locations = $prop->getLocations($pdo, $currentUser->userPropertyID);
    require('view/'.$router->currentPage);
} elseif ($router->currentPage == "verify.php") {
    require(ROOT.SLASH.'view'.SLASH.'verify.php');
} elseif (isset($router->param1)) {
    header('Location: /index.php');
} else {
    require('view/login.php');
}
