<?php
if (!defined('INAPP') || (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)) {
    header('Location: /index.php');
}
?>
<?php
    $login = new Login($pdo);
?>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="gaming streaming app" />
    <meta name="author" content="Jeff L. Simpson" />
    <meta name="keyword" content="responsive, bootstrap5" />
    <!--<base href="/" />-->
    <link rel="icon" href="view/img/innginelogo.png" type="image/x-icon" />
    <link href="view/css/main.min.css" rel="stylesheet" /> <!-- Bootstrap 5.0.0 Alpha 1 Style sheet -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="view/css/main.css" />
    <link rel="manifest" href="manifest.json">
    <script type="module">
        import 'https://cdn.jsdelivr.net/npm/@pwabuilder/pwaupdate';
        const el = document.createElement('pwa-update');
        document.body.appendChild(el);
    </script>
    <!-- Bootstrap Icons-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Righteous&display=swap">
    <title>Home</title>
</head>


<body class="login-modern" >
<div class="mainbg">
</div>
</body>




<!-- THIS BELOW SCRIPT CAUSES MULTIPLE PAGE REFRESHES AND I DON"T KNOW WHY YET -->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- Custom scripts
<script type="text/javascript"></script>
<script>
 if (!navigator.serviceWorker.controller) {
     navigator.serviceWorker.register("/sw.js").then(function(reg) {
         console.log("Service worker has been registered for scope: " + reg.scope);
     });
 }
</script>-->
</html>