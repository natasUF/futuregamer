<?php
if (!defined('INAPP')) {
    header('Location: /index.php');
}
//LOOKIT If we ever change hosting plans,  we must check our htaccess and see if ? is needed before query HttpQueryString
// https://stackoverflow.com/questions/14555996/no-input-file-specified


//Timezone  hb
date_default_timezone_set('America/New_York');
$todayDate = new DateTime();
// TODO  Create option for user to change timezone  Here is a list of America's
        //For the United States:

        //Eastern ........... America/New_York
        //Central ........... America/Chicago
        //Mountain .......... America/Denver
        //Mountain no DST ... America/Phoenix
        //Pacific ........... America/Los_Angeles
        //Alaska ............ America/Anchorage
        //Hawaii ............ America/Adak
        //Hawaii no DST ..... Pacific/Honolulu


//To Debug or not to debug
if (DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
}


//Cookie Vars
define('COOKIE_NAME', 'hash');
define('COOKIE_EXPIRY', '604800');

//Session settings
define('SESSION_TIMEOUT', 28800);  //28800 for an 8 hour shift
define('SESSION_REGENERATE', 36000);  //3600 for a new Session ID every 30 minutes
